# Renovate runner

This project contains the [Renovate runner](https://gitlab.com/renovate-bot/renovate-runner) that creates merge requests for dependency updates. It runs on scheduled pipelines.
